<footer>
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="ft__nav schedule">
                        <h4>Opening Hours</h4>
                        <ul>
                            <li><span>Monday</span> Closed</li>
                            <li><span>Tue - Fri</span> 10am - 9pm</li>
                            <li><span>Saturday</span> 10am - 9am</li>
                            <li><span>Sunday</span> 10am - 12pm</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="ft__nav ft-nav">
                        <h4>Navigation</h4>
                        <ul>
                            <li><a href="javascript:;">Home</a></li>
                            <li><a href="javascript:;">About</a></li>
                            <li><a href="javascript:;">Menu</a></li>
                            <li><a href="javascript:;">Deals</a></li>
                            <li><a href="javascript:;">FAQs</a></li>
                            <li><a href="javascript:;">Blog</a></li>
                            <li><a href="javascript:;">Contact</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="ft__nav social-icons">
                        <h4>Follow Us</h4>
                        <ul class="inline-block">
                            <li><a href="javascript:;"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
                            <li><a href="javascript:;"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <li><a href="javascript:;"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                    <div class="ft__nav ct-details">
                        <h4>Contact</h4>
                        <ul>
                            <li><a href="tel:123759654"><i class="fa fa-phone" aria-hidden="true"></i> +123759654</a></li>
                            <li><i class="fa fa-map-marker" aria-hidden="true"></i> 123B Bahrain Rd, <br />Bahrain</li>
                            <li><a href="javascript:;" class="btn-style-1">Order Now <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-4">
                    <div class="copyrights">
                        <p>copyrights</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="copyrights">
                        <img src="{{asset('webassets/assets/images/logo.png')}}" />
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="copyrights txt-right">
                        <p>2020</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="overlay"></div>
