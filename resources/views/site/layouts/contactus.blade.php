<section class="contact-home spacing__x bg__1 bg__detail">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="headingstyle1">
                    <h3>Contact Us</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat.</p>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="ct-details">
                            <ul>
                                <li>Phone
                                    <span><a href="tel:123789456">+123789456</a></span>
                                </li>
                                <li>Email
                                    <span><a href="mailto:example.com">example.com</a></span>
                                </li>
                                <li>Address
                                    <span><a href="javascript:;">123B Bahrain Rd, Bahrain</a></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input type="text" name="" placeholder="Email">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input type="text" name="" placeholder="Subject">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <textarea rows="10" placeholder="Message"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
