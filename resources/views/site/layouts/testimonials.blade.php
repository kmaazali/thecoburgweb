<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="headingstyle1">
                <h3>Testimonials</h3>
            </div>
        </div>
    </div>
    <div class="row testimonial-slider">
        <div class="col-lg-4">
            <div class="testimonial-box">
                <img src="{{asset('webassets/assets/images/review/1.jpg')}}">
                <h5>Brian Carter</h5>
                <h6>Owner Of Beehive</h6>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt</p>
                <ul class="inline-block">
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                </ul>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="testimonial-box">
                <img src="{{asset('webassets/assets/images/review/1.jpg')}}">
                <h5>Brian Carter</h5>
                <h6>Owner Of Beehive</h6>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt</p>
                <ul class="inline-block">
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                </ul>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="testimonial-box">
                <img src="{{asset('webassets/assets/images/review/1.jpg')}}">
                <h5>Brian Carter</h5>
                <h6>Owner Of Beehive</h6>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt</p>
                <ul class="inline-block">
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                </ul>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="testimonial-box">
                <img src="{{asset('webassets/assets/images/review/1.jpg')}}">
                <h5>Brian Carter</h5>
                <h6>Owner Of Beehive</h6>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt</p>
                <ul class="inline-block">
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                </ul>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="testimonial-box">
                <img src="{{asset('webassets/assets/images/review/1.jpg')}}">
                <h5>Brian Carter</h5>
                <h6>Owner Of Beehive</h6>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt</p>
                <ul class="inline-block">
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                </ul>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="testimonial-box">
                <img src="{{asset('webassets/assets/images/review/1.jpg')}}">
                <h5>Brian Carter</h5>
                <h6>Owner Of Beehive</h6>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt</p>
                <ul class="inline-block">
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                </ul>
            </div>
        </div>
    </div>
</div>
