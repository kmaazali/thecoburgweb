<header>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-2">
                <div class="logo">
                    <a href="./">
                        <img src="{{asset('webassets/assets/images/logo.png')}}">
                    </a>
                </div>
            </div>
            <div class="col-lg-10">
                <div class="main__navigation mob-menu">
                    <ul>
                        <li><a href="/">Home</a></li>
                        <li><a href="{{url('about')}}">About</a></li>
                        <li><a href="{{url('menu')}}">Menu</a></li>
                        <li><a href="{{url('faq')}}">FAQ's</a></li>
                        <li><a href="{{url('testimonial')}}">Testimonial</a></li>
                        <li><a href="{{url('contact')}}">Contact</a></li>
                        <li><a href="javascript:;"><i class="fa fa-shopping-cart"></i></a></li>
                        <li><a href="javascript:;"><i class="fa fa-user"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
