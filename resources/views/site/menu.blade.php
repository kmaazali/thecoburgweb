<!DOCTYPE html>
<html lang="en">
<head>
    <title>Menu | The Coburg Pizza</title>
    @include('site.layouts.compatibility')
    @include('site.layouts.style')
</head>
<body>

@include('site.layouts.header')

<section class="banner-home h-80 bg__detail" style="background-image: url({{asset('webassets/assets/images/banner/banner-menu.jpg')}});">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-4">
                <div class="banner-title">
                    <h2>Menu</h2>
                    <ul class="breadcrums">
                        <li><a href="javascript:;">Home</a></li> /
                        <li><a href="javascript:;">Menu</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="about-sctions bg__detail spacing__x">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="headingstyle1">
                    <h3>The Story Behaind Our Food</h3>
                    <p>English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="bg__1 spacing__x">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="menu-item-tabs">
                    <ul>

                        <li data-targetit="box-item-1" class="active"><a href="javascript:;">Appetizers</a></li>
                        <li data-targetit="box-item-2"><a href="javascript:;">Stir Fry</a></li>
                        <li data-targetit="box-item-3"><a href="javascript:;">Grill & Seafood</a></li>
                        <li data-targetit="box-item-4"><a href="javascript:;">Sizzling</a></li>
                        <li data-targetit="box-item-5"><a href="javascript:;">Dessert</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="box-item-1 showfirst">
                    <div class="main-item-list-box">
                        <div class="type-main-header">
                            <h6>Appetizers</h6>
                            <img src="{{asset('webassets/assets/images/menu/item-1/1.jpg')}}">
                        </div>
                        <div class="type-menu-items">
                            <ul>
                                <li>
                                    <a href="javascript:;" class="order-now">
                                        <div class="item-image">
                                            <img src="{{asset('webassets/assets/images/menu/item-1/item/1.jpg')}}">
                                        </div>
                                        <div class="item-title">
                                            <h6 class="item-name">Item Name</h6>
                                            <p class="item-detail">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem</p>
                                        </div>
                                        <div class="item-order">
                                            <div class="order-detail">
                                                <h5 class="item-price"><span>BD</span> 20.00</h5>
                                                <h6 class="item-name">Chicken</h6>
                                            </div>
                                            <div class="order-detail">
                                                <h5 class="item-price"><span>BD</span> 25.00</h5>
                                                <h6 class="item-name">Prawn & Calamari</h6>
                                            </div>
                                            <div class="order-detail">
                                                <h5 class="item-price"><span>BD</span> 23.00</h5>
                                                <h6 class="item-name">Beef</h6>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;" class="order-now">
                                        <div class="item-image">
                                            <img src="{{asset('webassets/assets/images/menu/item-1/item/2.jpg')}}">
                                        </div>
                                        <div class="item-title">
                                            <h6 class="item-name">Item Name</h6>
                                            <p class="item-detail">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem</p>
                                        </div>
                                        <div class="item-order">
                                            <div class="order-detail">
                                                <h5 class="item-price"><span>BD</span> 20.00</h5>
                                                <h6 class="item-name">Chicken</h6>
                                            </div>
                                            <div class="order-detail">
                                                <h5 class="item-price"><span>BD</span> 25.00</h5>
                                                <h6 class="item-name">Prawn & Calamari</h6>
                                            </div>
                                            <div class="order-detail">
                                                <h5 class="item-price"><span>BD</span> 23.00</h5>
                                                <h6 class="item-name">Beef</h6>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;" class="order-now">
                                        <div class="item-image">
                                            <img src="{{asset('webassets/assets/images/menu/item-1/item/1.jpg')}}">
                                        </div>
                                        <div class="item-title">
                                            <h6 class="item-name">Item Name</h6>
                                            <p class="item-detail">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem</p>
                                        </div>
                                        <div class="item-order">
                                            <div class="order-detail">
                                                <h5 class="item-price"><span>BD</span> 20.00</h5>
                                                <h6 class="item-name">Chicken</h6>
                                            </div>
                                            <div class="order-detail">
                                                <h5 class="item-price"><span>BD</span> 25.00</h5>
                                                <h6 class="item-name">Prawn & Calamari</h6>
                                            </div>
                                            <div class="order-detail">
                                                <h5 class="item-price"><span>BD</span> 23.00</h5>
                                                <h6 class="item-name">Beef</h6>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;" class="order-now">
                                        <div class="item-image">
                                            <img src="{{asset('webassets/assets/images/menu/item-1/item/2.jpg')}}">
                                        </div>
                                        <div class="item-title">
                                            <h6 class="item-name">Item Name</h6>
                                            <p class="item-detail">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem</p>
                                        </div>
                                        <div class="item-order">
                                            <div class="order-detail">
                                                <h5 class="item-price"><span>BD</span> 20.00</h5>
                                                <h6 class="item-name">Chicken</h6>
                                            </div>
                                            <div class="order-detail">
                                                <h5 class="item-price"><span>BD</span> 25.00</h5>
                                                <h6 class="item-name">Prawn & Calamari</h6>
                                            </div>
                                            <div class="order-detail">
                                                <h5 class="item-price"><span>BD</span> 23.00</h5>
                                                <h6 class="item-name">Beef</h6>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="box-item-2">
                    <div class="main-item-list-box">
                        <div class="type-main-header">
                            <h6>Stir Fry</h6>
                            <img src="{{asset('webassets/assets/images/menu/item-2/1.jpg')}}">
                        </div>
                        <div class="type-menu-items">
                            <ul>
                                <li>
                                    <a href="javascript:;" class="order-now">
                                        <div class="item-image">
                                            <img src="{{asset('webassets/assets/images/menu/item-1/item/1.jpg')}}">
                                        </div>
                                        <div class="item-title">
                                            <h6 class="item-name">Item Name</h6>
                                            <p class="item-detail">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem</p>
                                        </div>
                                        <div class="item-order">
                                            <div class="order-detail">
                                                <h5 class="item-price"><span>BD</span> 20.00</h5>
                                                <h6 class="item-name">Chicken</h6>
                                            </div>
                                            <div class="order-detail">
                                                <h5 class="item-price"><span>BD</span> 25.00</h5>
                                                <h6 class="item-name">Prawn & Calamari</h6>
                                            </div>
                                            <div class="order-detail">
                                                <h5 class="item-price"><span>BD</span> 23.00</h5>
                                                <h6 class="item-name">Beef</h6>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;" class="order-now">
                                        <div class="item-image">
                                            <img src="{{asset('webassets/assets/images/menu/item-1/item/2.jpg')}}">
                                        </div>
                                        <div class="item-title">
                                            <h6 class="item-name">Item Name</h6>
                                            <p class="item-detail">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem</p>
                                        </div>
                                        <div class="item-order">
                                            <div class="order-detail">
                                                <h5 class="item-price"><span>BD</span> 20.00</h5>
                                                <h6 class="item-name">Chicken</h6>
                                            </div>
                                            <div class="order-detail">
                                                <h5 class="item-price"><span>BD</span> 25.00</h5>
                                                <h6 class="item-name">Prawn & Calamari</h6>
                                            </div>
                                            <div class="order-detail">
                                                <h5 class="item-price"><span>BD</span> 23.00</h5>
                                                <h6 class="item-name">Beef</h6>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;" class="order-now">
                                        <div class="item-image">
                                            <img src="{{asset('webassets/assets/images/menu/item-1/item/1.jpg')}}">
                                        </div>
                                        <div class="item-title">
                                            <h6 class="item-name">Item Name</h6>
                                            <p class="item-detail">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem</p>
                                        </div>
                                        <div class="item-order">
                                            <div class="order-detail">
                                                <h5 class="item-price"><span>BD</span> 20.00</h5>
                                                <h6 class="item-name">Chicken</h6>
                                            </div>
                                            <div class="order-detail">
                                                <h5 class="item-price"><span>BD</span> 25.00</h5>
                                                <h6 class="item-name">Prawn & Calamari</h6>
                                            </div>
                                            <div class="order-detail">
                                                <h5 class="item-price"><span>BD</span> 23.00</h5>
                                                <h6 class="item-name">Beef</h6>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;" class="order-now">
                                        <div class="item-image">
                                            <img src="{{asset('webassets/assets/images/menu/item-1/item/2.jpg')}}">
                                        </div>
                                        <div class="item-title">
                                            <h6 class="item-name">Item Name</h6>
                                            <p class="item-detail">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem</p>
                                        </div>
                                        <div class="item-order">
                                            <div class="order-detail">
                                                <h5 class="item-price"><span>BD</span> 20.00</h5>
                                                <h6 class="item-name">Chicken</h6>
                                            </div>
                                            <div class="order-detail">
                                                <h5 class="item-price"><span>BD</span> 25.00</h5>
                                                <h6 class="item-name">Prawn & Calamari</h6>
                                            </div>
                                            <div class="order-detail">
                                                <h5 class="item-price"><span>BD</span> 23.00</h5>
                                                <h6 class="item-name">Beef</h6>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="box-item-3"></div>
                <div class="box-item-4"></div>
                <div class="box-item-5"></div>
            </div>
        </div>
    </div>
</section>

<div class="sidebar-filter">
    <div class="side-head">Filters</div>
    <div class="side-body">
        <a href="javascript:;" class="close-filter">x</a>
        <h5>Filters</h5>
        <div class="clear-all"><a href="javascript:;">Clear All</a></div>
        <ul class="item-selected">
            <li><a href="javascript:;">Beef</a></li>
            <li><a href="javascript:;">Spicy</a></li>
            <li><a href="javascript:;">Prawn</a></li>
            <li><a href="javascript:;">Calamari</a></li>
            <li><a href="javascript:;">Duck</a></li>
            <li><a href="javascript:;">Calamari</a></li>
        </ul>
    </div>
</div>

<div class="popup popup-default">
    <a href="javascript:;" class="popup-close">x</a>
    <div class="row align-items-center">
        <div class="col-lg-5">
            <div class="img__area">
                <img src="{{asset('webassets/assets/images/menu/item-1/full/1.jpg')}}">
            </div>
        </div>
        <div class="col-lg-7 popup-padding">
            <div class="row">
                <div class="col-lg-7">
                    <div class="popup-item-name">
                        <h6 class="item-name">Item Name</h6>
                        <p class="item-detail">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem</p>
                    </div>
                </div>
                <div class="col-lg-5 type-menu-items">
                    <div class="item-order">
                        <div class="order-detail">
                            <h5 class="item-price"><span>BD</span> 20.00</h5>
                            <h6 class="item-name">Chicken</h6>
                        </div>
                        <div class="order-detail">
                            <h5 class="item-price"><span>BD</span> 25.00</h5>
                            <h6 class="item-name">Prawn &amp; Calamari</h6>
                        </div>
                        <div class="order-detail">
                            <h5 class="item-price"><span>BD</span> 23.00</h5>
                            <h6 class="item-name">Beef</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="item-selected">
                        <ul>
                            <li><a href="javascript:;">Beef</a></li>
                            <li><a href="javascript:;">Spicy</a></li>
                            <li><a href="javascript:;">Prawn</a></li>
                            <li><a href="javascript:;">Calamari</a></li>
                            <li><a href="javascript:;">Duck</a></li>
                        </ul>
                    </div>
                </div>
                <div class="item-value">
                    <h5>Nutritinol Value</h5>
                    <div class="table-head">
                        <ul class="dtbl">
                            <li class="dtd">Calories</li>
                            <li class="dtd">Facts</li>
                            <li class="dtd">Sturated Facts</li>
                            <li class="dtd">Carbs</li>
                            <li class="dtd">Sodium</li>
                        </ul>
                    </div>
                    <div class="table-body">
                        <ul class="dtbl">
                            <li class="dtd">710</li>
                            <li class="dtd">27g</li>
                            <li class="dtd">6g</li>
                            <li class="dtd">40g</li>
                            <li class="dtd">1830mg</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('site.layouts.footer')
<script src="{{asset('webassets/assets/js/plugin.js')}}"></script>
<script src="{{asset('webassets/assets/js/custom.js')}}"></script>

</body>
</html>
