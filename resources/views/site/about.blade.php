<!DOCTYPE html>
<html lang="en">
<head>
    <title>About Us | The Coburg Pizza</title>
    @include('site.layouts.compatibility')
    @include('site.layouts.style')
</head>
<body>
@include('site.layouts.header')

<section class="banner-home h-80 bg__detail" style="background-image: url({{asset('webassets/assets/images/banner/banner-aboutus.jpg')}});">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-4">
                <div class="banner-title">
                    <h2>About Us</h2>
                    <ul class="breadcrums">
                        <li><a href="javascript:;">Home</a></li> /
                        <li><a href="javascript:;">About Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="about-sctions bg__1 bg__detail spacing__x">
    <div class="container">
        <div class="row">
            <div class="col-lg-1"></div>
            <div class="col-lg-10">
                <div class="text__area">
                    <div class="headingstyle1">
                        <h3>The Story Behaind Our Food</h3>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters <br /> <br />English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="element element-1">
        <img src="{{asset('webassets/assets/images/mix/pz-2.png')}}">
    </div>
</section>

<section class="story-area bg-color-1">
    <div class="container-fluid p-0">
        <div class="row align-items-center">
            <div class="col-lg-6 p-0">
                <div class="text__area">
                    <h3>Company Mansion</h3>
                    <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC</p>
                    <img src="{{asset('webassets/assets/images/elements/element-2.png')}}" />
                </div>
            </div>
            <div class="col-lg-6 p-0">
                <div class="img__area">
                    <img src="{{asset('webassets/assets/images/mix/img-1.jpg')}}">
                </div>
            </div>
        </div>
    </div>
</section>
<section class="story-area bg-color-1">
    <div class="container-fluid p-0">
        <div class="row align-items-center">
            <div class="col-lg-6 p-0">
                <div class="img__area">
                    <img src="{{asset('webassets/assets/images/mix/img-2.jpg')}}">
                </div>
            </div>
            <div class="col-lg-6 p-0">
                <div class="text__area">
                    <h3>Key Service Provider</h3>
                    <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC</p>
                    <img src="{{asset('webassets/assets/images/elements/element-2.png')}}" />
                </div>
            </div>
        </div>
    </div>
</section>
<section class="story-area bg-color-1">
    <div class="container-fluid p-0">
        <div class="row align-items-center">
            <div class="col-lg-7 p-0">
                <div class="text__area">
                    <h3>The Founder's Word</h3>
                    <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC</p>
                    <img src="{{asset('webassets/assets/images/elements/element-2.png')}}" />
                </div>
            </div>
            <div class="col-lg-5 p-0">
                <div class="img__area">
                    <img src="{{asset('webassets/assets/images/mix/founder.jpg')}}">
                </div>
            </div>
        </div>
    </div>
</section>
@include('site.layouts.footer')

<script src="{{asset('webassets/assets/js/plugin.js')}}"></script>
<script src="{{asset('webassets/assets/js/custom.js')}}"></script>

</body>
</html>
