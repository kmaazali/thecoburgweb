<!DOCTYPE html>
<html lang="en">
<head>
    <title>Home | The Coburg Pizza</title>
    @include('site.layouts.compatibility')
    @include('site.layouts.style')
</head>
<body>

@include('site.layouts.header')

<section class="banner-home h-80 bg__detail" style="background-image: url({{asset('webassets/assets/images/banner/banner-contactus.jpg')}});">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-4">
                <div class="banner-title">
                    <h2>Contact Us</h2>
                    <ul class="breadcrums">
                        <li><a href="javascript:;">Home</a></li> /
                        <li><a href="javascript:;">Contact Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

@include('site.layouts.contactus')
@include('site.layouts.map')

@include('site.layouts.footer')

<script src="{{asset('webassets/assets/js/plugin.js')}}"></script>
<script src="{{asset('webassets/assets/js/custom.js')}}"></script>

</body>
</html>
