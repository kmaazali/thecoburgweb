<!DOCTYPE html>
<html lang="en">
<head>
    <title>About Us | The Coburg Pizza</title>
    @include('site.layouts.compatibility')
    @include('site.layouts.style')
</head>
<body class="testimonial-nav">

@include('site.layouts.header')

<section class="banner-home h-80 bg__detail" style="background-image: url({{asset('webassets/assets/images/bg/bg-1.jpg')}});">
    @include('site.layouts.testimonials')
</section>

@include('site.layouts.footer')

<script src="{{asset('webassets/assets/js/plugin.js')}}"></script>
<script src="{{asset('webassets/assets/js/custom.js')}}"></script>

</body>
</html>
