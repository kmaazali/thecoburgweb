<!DOCTYPE html>
<html lang="en">
<head>
    <title>Home | The Coburg Pizza</title>
    @include('site.layouts.compatibility')
    @include('site.layouts.style')
</head>
<body>

@include('site.layouts.header')

<section class="banner-home bg__detail" style="background-image: url({{asset('webassets/assets/images/banner/banner-home.jpg')}});">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="banner-content">
                    <img src="{{asset('webassets/assets/images/logo.png')}}">
                    <a href="javascript:;" class="btn-style-1">Order Online <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="testimonials bg__1 bg__detail spacing__x">
    @include('site.layouts.testimonials')
</section>

<section class="bg__detail spacing__x" style="background-image: url({{asset('webassets/assets/images/bg/about-service.jpg')}});">
    <div class="container=fluid">
        <div class="row">
            <div class="col-lg-4">
                <div class="about__service">
                    <img src="{{asset('webassets/assets/images/mix/pz-1.png')}}">
                    <h4>100%</h4>
                    <p>Premimun Quality</p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="about__service">
                    <img src="{{asset('webassets/assets/images/mix/pz-1.png')}}">
                    <h4>1000</h4>
                    <p>Bowls Sold</p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="about__service">
                    <img src="{{asset('webassets/assets/images/mix/pz-1.png')}}">
                    <h4>100%</h4>
                    <p>Authentic Thai</p>
                </div>
            </div>
        </div>
    </div>
</section>

@include('site.layouts.contactus')

@include('site.layouts.map')

@include('site.layouts.footer')

<script src="{{asset('webassets/assets/js/plugin.js')}}"></script>
<script src="{{asset('webassets/assets/js/custom.js')}}"></script>

</body>
</html>
