<!DOCTYPE html>
<html lang="en">
<head>
    <title>FAQ's | The Coburg Pizza</title>
    @include('site.layouts.compatibility')
    @include('site.layouts.style')
</head>
<body>

@include('site.layouts.header')

<section class="banner-home h-80 bg__detail" style="background-image: url({{asset('webassets/assets/images/banner/banner-faq.jpg')}});">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-4">
                <div class="banner-title">
                    <h2>FAQ's</h2>
                    <ul class="breadcrums">
                        <li><a href="javascript:;">Home</a></li> /
                        <li><a href="javascript:;">FAQ's</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="about-sctions bg__1 bg__detail spacing__x">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="accordianBox">
                    <ul class="accordian">
                        <li class="first">
                            <h4>Submit Your Career Brief</h4>
                            <div style="display: none;">
                                <p>Submit your detailed career brief to start-off your project. Our certified resume writers are eager to tailor your resume to set the professional tone.</p>
                            </div>
                        </li>
                        <li class="">
                            <h4>Get Your First Resume Draft</h4>
                            <div style="display: none;">
                                <p>Our resume specialists write an up-to-date resume that can fluently pass through the Applicant Tracking System Scan and make your resume get grabbed.</p>
                            </div>
                        </li>
                        <li class="active">
                            <h4>Finalize Your Resume Document</h4>
                            <div style="display: block;">
                                <p>Review your draft and request for free revision, if any. Finalize your resume draft after being completely satisfied to get your final files through your client area.</p>
                            </div>
                        </li>
                        <li class="last">
                            <h4>Download Your Final Resume Files</h4>
                            <div style="">
                                <p>Download final files of your resume through your dashboard. Now you are ready to apply for your dream job with a professionally written resume.</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

@include('site.layouts.footer')

<script src="{{asset('webassets/assets/js/plugin.js')}}"></script>
<script src="{{asset('webassets/assets/js/custom.js')}}"></script>

</body>
</html>
