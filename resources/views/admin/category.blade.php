<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
@include('admin.layouts.header')
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">

<!--Modal-->
<!-- Modal -->
<div class="modal fade" id="addnewcategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="post" action="{{url('admin/addnewcategory')}}" enctype="multipart/form-data">
                {{csrf_field()}}
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add New Category</h5>
            </div>
            <div class="modal-body">

                    <div class="form-group">
                        <label>Category Name</label>
                        <input class="form-control" type="text" required name="categoryname" id="categoryname"/>
                    </div>
                    <div class="form-group">
                        <label>Category Image</label>
                        <input class="form-control" type="file" required name="categoryimage" id="categoryimage" accept="image/x-png,image/gif,image/jpeg"/>
                    </div>
                    <div class="form-group">
                        <label>Category Description</label>
                        <textarea name="categorydescription" id="categorydescription"></textarea>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!--End Modal-->
<!-- BEGIN HEADER -->
<div id="header" class="navbar navbar-inverse navbar-fixed-top">
    <!-- BEGIN TOP NAVIGATION BAR -->
    <div class="navbar-inner">
        <div class="container-fluid">
            <!--BEGIN SIDEBAR TOGGLE-->
            <div class="sidebar-toggle-box hidden-phone">
                <div class="icon-reorder"></div>
            </div>
            <!--END SIDEBAR TOGGLE-->
            <!-- BEGIN LOGO -->
            <a class="brand" href="{{url('admin/menu')}}">
                <img src="{{asset('assets/img/logo.png')}}" alt="Metro Lab" />
            </a>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="arrow"></span>
            </a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- END  NOTIFICATION -->
        </div>
    </div>
    <!-- END TOP NAVIGATION BAR -->
</div>
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

            <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
            <div class="navbar-inverse">
                <form class="navbar-search visible-phone">
                    <input type="text" class="search-query" placeholder="Search" />
                </form>
            </div>
            <!-- END RESPONSIVE QUICK SEARCH FORM -->
            <!-- BEGIN SIDEBAR MENU -->
            <ul class="sidebar-menu">
                <li class="sub-menu">
                    <a class="" href="{{url('admin/home')}}">
                        <i class="icon-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a class="" href="{{url('admin/menu')}}">
                        <i class="icon-dashboard"></i>
                        <span>Menu</span>
                    </a>
                </li>
                <li class="sub-menu active">
                    <a class="" href="{{url('admin/category')}}">
                        <i class="icon-dashboard"></i>
                        <span>Category</span>
                    </a>
                </li>
            </ul>
            <!-- END SIDEBAR MENU -->
        </div>
    </div>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span12" style="display: inline-flex;">
                    <h3 class="page-title">Category Items</h3>
                    <button class="pull-12 btn btn-primary" data-toggle="modal" data-target="#addnewcategory">Add New Category</button>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN EXAMPLE TABLE widget-->
                    <div class="widget red">
                        <div class="widget-title">
{{--                            <h4><i class="icon-reorder"></i> Dynamic Table</h4>--}}
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                                <a href="javascript:;" class="icon-remove"></a>
                            </span>
                        </div>
                        <div class="widget-body">
                            <div id="sample_1_wrapper" class="dataTables_wrapper form-inline" role="grid"><div class="row-fluid"><div class="span6"><div id="sample_1_length" class="dataTables_length"><label><select size="1" name="sample_1_length" aria-controls="sample_1" class="input-mini"><option value="10" selected="selected">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> records per page</label></div></div><div class="span6"><div class="dataTables_filter" id="sample_1_filter"><label>Search: <input type="text" aria-controls="sample_1" class="input-medium"></label></div></div></div><table class="table table-striped table-bordered dataTable" id="sample_1" aria-describedby="sample_1_info">
                                    <thead>
                                    <tr role="row">
                                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="Username: activate to sort column ascending" style="width: 226px;">Category Name</th>
                                        <th class="hidden-phone sorting" role="columnheader" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="Email: activate to sort column ascending" style="width: 433px;">Category Image</th>
                                        <th class="hidden-phone sorting" role="columnheader" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="Points: activate to sort column ascending" style="width: 156px;">Category Description</th>
                                        <th class="hidden-phone sorting" role="columnheader" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="Joined: activate to sort column ascending" style="width: 236px;">Action</th>
                                    </tr>
                                    </thead>

                                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                                    @foreach($category as $cat)
                                    <tr class="gradeX odd">

                                            <td>{{$cat->category_name}}</td>
                                            <td>{{$cat->category_image}}</td>
                                            <td>{{$cat->category_description}}</td>
                                            <td><a href="{{url('admin/deletecategory/'.$cat->id)}}">Delete</a></td>
                                    </tr>
                                    @endforeach

                                    </tbody></table>
                                <div class="row-fluid"><div class="span6">
                                        {{$category->links()}}
                                </div></div>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>
        </div>
    </div>
</div>


<!-- BEGIN FOOTER -->
<div id="footer">
    2013 &copy; Metro Lab Dashboard.
</div>
<!-- END FOOTER -->
@include('admin.layouts.footer')
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>

