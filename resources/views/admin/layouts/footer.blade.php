
<!-- BEGIN JAVASCRIPTS -->
<!-- Load javascripts at bottom, this will reduce page load time -->
<script src="{{asset('assets/js/jquery-1.8.3.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.nicescroll.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('assets/assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/assets/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('assets/assets/fullcalendar/fullcalendar/fullcalendar.min.js')}}"></script>
<script src="{{asset('assets/assets/bootstrap/js/bootstrap.min.js')}}"></script>

<!-- ie8 fixes -->
<!--[if lt IE 9]>
<script src="{{asset('assets/js/excanvas.js')}}"></script>
<script src="{{asset('assets/js/respond.js')}}"></script>
<![endif]-->

<script src="{{asset('assets/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/jquery.sparkline.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/assets/chart-master/Chart.js')}}"></script>

<!--common script for all pages-->
<script src="{{asset('assets/js/common-scripts.js')}}"></script>

<!--script for this page only-->

<script src="{{asset('assets/js/easy-pie-chart.js')}}"></script>
<script src="{{asset('assets/js/sparkline-chart.js')}}"></script>
<script src="{{asset('assets/js/home-page-calender.js')}}"></script>
<script src="{{asset('assets/js/chartjs.js')}}"></script>
