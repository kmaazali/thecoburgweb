<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
@include('admin.layouts.header')
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">

<!-- Modal -->
<div class="modal fade" id="addnewitem" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="post" action="{{url('admin/addproduct')}}" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Add New Product</h5>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label>Product Name</label>
                        <input class="form-control" type="text" required name="product_name" id="product_name"/>
                    </div>
                    <div class="form-group">
                        <label>Category Name</label>
                        <select name="category_id" id="category_id">
                            @foreach($category as $cat)
                                <option value="{{$cat->id}}">{{$cat->category_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Product Description</label>
                        <textarea name="product_description" id="product_description"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Product Image</label>
                        <input type="file" name="product_image" id="product_image"/>
                    </div>
                    <div class="form-group">
                        <label>Product Price</label>
                        <input type="text" name="product_price" id="product_price"/>
                    </div>
                    <div class="form-group">
                        <label>Product Preparation Time</label>
                        <input type="text" name="product_preparation_time" id="product_preparation_time"/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--End Modal-->
<!-- BEGIN HEADER -->
<div id="header" class="navbar navbar-inverse navbar-fixed-top">
    <!-- BEGIN TOP NAVIGATION BAR -->
    <div class="navbar-inner">
        <div class="container-fluid">
            <!--BEGIN SIDEBAR TOGGLE-->
            <div class="sidebar-toggle-box hidden-phone">
                <div class="icon-reorder"></div>
            </div>
            <!--END SIDEBAR TOGGLE-->
            <!-- BEGIN LOGO -->
            <a class="brand" href="{{url('admin/menu')}}">
                <img src="{{asset('assets/img/logo.png')}}" alt="Metro Lab" />
            </a>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="arrow"></span>
            </a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- END  NOTIFICATION -->
        </div>
    </div>
    <!-- END TOP NAVIGATION BAR -->
</div>
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

            <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
            <div class="navbar-inverse">
                <form class="navbar-search visible-phone">
                    <input type="text" class="search-query" placeholder="Search" />
                </form>
            </div>
            <!-- END RESPONSIVE QUICK SEARCH FORM -->
            <!-- BEGIN SIDEBAR MENU -->
            <ul class="sidebar-menu">
                <li class="sub-menu">
                    <a class="" href="{{url('admin/home')}}">
                        <i class="icon-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="sub-menu active">
                    <a class="" href="{{url('admin/menu')}}">
                        <i class="icon-dashboard"></i>
                        <span>Menu</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a class="" href="{{url('admin/category')}}">
                        <i class="icon-dashboard"></i>
                        <span>Category</span>
                    </a>
                </li>
            </ul>
            <!-- END SIDEBAR MENU -->
        </div>
    </div>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
<div class="row-fluid">
    <div class="span12">
        <h3 class="page-title">Menu Items</h3>
        <button class="pull-12 btn btn-primary" data-toggle="modal" data-target="#addnewitem">Add New Product</button>
    </div>
</div>
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE widget-->
        <div class="widget red">
            <div class="widget-title">
{{--                <h4><i class="icon-reorder"></i> Dynamic Table</h4>--}}
                <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                                <a href="javascript:;" class="icon-remove"></a>
                            </span>
            </div>
            <div class="widget-body">
                <div id="sample_1_wrapper" class="dataTables_wrapper form-inline" role="grid"><div class="row-fluid"><div class="span6"><div id="sample_1_length" class="dataTables_length"><label><select size="1" name="sample_1_length" aria-controls="sample_1" class="input-mini"><option value="10" selected="selected">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> records per page</label></div></div><div class="span6"><div class="dataTables_filter" id="sample_1_filter"><label>Search: <input type="text" aria-controls="sample_1" class="input-medium"></label></div></div></div><table class="table table-striped table-bordered dataTable" id="sample_1" aria-describedby="sample_1_info">
                        <thead>
                        <tr role="row"><th style="width: 13px;" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label=""><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes"></th>
                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="Username: activate to sort column ascending" style="width: 226px;">Product Name</th>
                            <th class="hidden-phone sorting" role="columnheader" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="Email: activate to sort column ascending" style="width: 433px;">Category</th>
                            <th class="hidden-phone sorting" role="columnheader" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="Points: activate to sort column ascending" style="width: 156px;">Product Price</th>
                            <th class="hidden-phone sorting" role="columnheader" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="Joined: activate to sort column ascending" style="width: 236px;">Product Status</th>
                            <th class="hidden-phone sorting" role="columnheader" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label=": activate to sort column ascending" style="width: 261px;"></th>
                        </tr>
                        </thead>

                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                        @foreach($products as $product)
                        <tr class="odd gradeX even">
                            <td class="sorting_1"><input type="checkbox" class="checkboxes" value="1"></td>
                            <td class="">{{$product->product_name}}</td>
                            <td class="hidden-phone ">{{$product->category->category_name}}</td>
                            <td class="hidden-phone ">{{$product->product_price}}</td>
                            <td class="center hidden-phone ">{{$product->product_status}}</td>
                            <td class="hidden-phone "><span class="label label-success"><a href="#">Delete</a></span></td>
                        </tr>
                        @endforeach
                        </tbody></table><div class="row-fluid"><div class="span6"><div class="dataTables_info" id="sample_1_info">Showing 1 to 10 of 25 entries</div></div><div class="span6"><div class="dataTables_paginate paging_bootstrap pagination"><ul><li class="prev disabled"><a href="#">← Prev</a></li><li class="active"><a href="#">1</a></li><li><a href="#">2</a></li><li><a href="#">3</a></li><li class="next"><a href="#">Next → </a></li></ul></div></div></div></div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE widget-->
    </div>
</div>
        </div>
    </div>
</div>


<!-- BEGIN FOOTER -->
<div id="footer">
    2013 &copy; Metro Lab Dashboard.
</div>
<!-- END FOOTER -->
@include('admin.layouts.footer')

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>

