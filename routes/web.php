<?php

use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\SiteController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',function (){
   return view('site.index');
});
//Route::get('/menu',function (){
//    return view('site.menu');
//});

Route::get('/menu',[SiteController::class,'getmenupage']);
Route::get('/about',function (){
    return view('site.about');
});
Route::get('/contact',function (){
   return view('site.contact');
});
Route::get('/faq',function (){
    return view('site.faq');
});
Route::get('/testimonial',function (){
    return view('site.testimonial');
});
Route::get('/admin', function () {
    return view('admin.login');
});
Route::post('dologin',[AuthController::class,'dologin']);

Route::group(['middleware' => 'auth','prefix'=>'admin'], function () {
    // User needs to be authenticated to enter here.
    Route::get('/home',[AdminController::class,'homepage']);
    Route::get('/menu',[AdminController::class,'menupage']);
    Route::post('/addproduct',[AdminController::class,'addproduct']);
    Route::get('/category',[AdminController::class,'categorypage']);
    Route::post('/addnewcategory',[AdminController::class,'addnewcategory']);
    Route::get('/deletecategory/{id}',[AdminController::class,'deletecategory']);

    Route::get('user/profile', function () {
        // Uses Auth Middleware
    });
});
