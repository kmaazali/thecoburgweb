<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function getmenupage(){
        $products = Product::with('category')->orderBy('category_id','ASC')->get();
        $category = ProductCategory::get();
        return view('site.menu',with(['products'=>$products,'category'=>$category]));
    }
}
