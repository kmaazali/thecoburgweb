<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function homepage(){
        return view('admin.home');
    }
    public function menupage(){
        $products = Product::with('category')->get();
        $category = ProductCategory::get();
        return view('admin.menu',with(['products'=>$products,'category'=>$category]));
    }
    public function categorypage(){
        $category = ProductCategory::paginate(10);
        return view('admin.category',with(['category'=>$category]));
    }
    public function addnewcategory(Request $request){
        $category = new ProductCategory();
        $category->category_name = $request->categoryname;
        if($request->hasFile('categoryimage')){
            $fileName = time().'.'.$request->categoryimage->extension();

            $request->categoryimage->move(storage_path('uploads'), $fileName);
            $category->category_image = $fileName;
        }
        $category->category_description = $request->categorydescription;
        $category->save();
        $category = ProductCategory::paginate(10);
        return view('admin.category',with(['category'=>$category]));
    }

    public function addproduct(Request $request){
        $product = new Product();
        if($request->hasFile('product_image')){
            $fileName = time().'.'.$request->product_image->extension();

            $request->product_image->move(storage_path('uploads'), $fileName);
            $product->product_image = $fileName;
        }
        $product->category_id = $request->category_id;
        $product->product_name = $request->product_name;
        $product->product_description = $request->product_description;
        $product->product_price = $request->product_price;
        $product->product_preparation_time = $request->product_preparation_time;
        $product->product_status = $request->product_status;
        $product->save();
        $product = Product::paginate(10);
        return view('admin.menu',with(['product'=>$product]));
    }

    public function deletecategory($id){
        ProductCategory::where('id',$id)->delete();
        $category = ProductCategory::paginate(10);
        return view('admin.category',with(['category'=>$category]));
    }
}
