<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductAddons extends Model
{
    use HasFactory;
    protected $table = 'product_addons';

    protected $fillable = [
        'product_id',
        'add_on_name',
        'add_on_price',
        'add_on_description'
    ];

    public function product(){
        return $this->belongsTo(Product::class,'product_id','id');
    }
}
