<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = 'products';

    protected $fillable = [
      'category_id',
      'product_name',
      'product_description',
      'product_image',
      'product_price',
      'product_preparation_time',
      'product_status'
    ];

    public function category(){
        return $this->belongsTo(ProductCategory::class,'category_id','id');
    }
}
