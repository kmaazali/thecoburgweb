<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $table = 'orders';

    protected $fillable = [
      'user_id',
      'order_time',
      'order_status',
      'order_waiting_time',
      'order_price',
      'delivery_fee',
      'order_type'
    ];

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
}
