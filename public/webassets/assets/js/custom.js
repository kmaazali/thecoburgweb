$(document).ready(function() {

	/* Tabbing Function */
    $('[data-targetit]').on('click', function(e) {
        $(this).addClass('active');
        $(this).siblings().removeClass('active');
        var target = $(this).data('targetit');
        $('.' + target).siblings('[class^="box-"]').hide();
        $('.' + target).fadeIn();
    });

    $('.side-head').on('click', function(){
        $(this).parent().toggleClass('open');
    });

    $('.order-now').on('click', function(){
        $('.popup-default').fadeIn();
        $('.overlay').fadeIn();
        $('body').addClass('over-hidden');
    });
    $('.popup-close').on('click', function(){
        closePopup();
    });

    // Accordian
    $('.accordian h4').click(function() {
        $('.accordian li').removeClass('active');
        $(this).parent('li').addClass('active');
        $('.accordian li div').slideUp();
        $(this).parent('li').find('div').slideDown();
    });
    
	$('.testimonial-slider').slick({
		dots: false,
		arrows: true,
		infinite: true,
		speed: 300,
		slidesToShow: 3,
		slideToScroll: 1,
		centerMode: true,
		centerPadding: '60px',
	});


});


$(window).scroll(function(){
    if ($(window).scrollTop() >= 50) {
        $('header').addClass('sticky-header');
    }
    else {
        $('header').removeClass('sticky-header');
    }
});

function closePopup() {
    $('.popup').hide();
    $('body').removeClass('over-hidden');
    $('.overlay').fadeOut();
}